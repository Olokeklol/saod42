﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod42
{
    public enum Mark { Black, Red }

    class Node
    {
        private Node right;
        private Node left;
        private Node parent;
        private int data;

        public Mark color;

        public Node()
        {
            parent = null;
            right = null;
            left = null;
        }
        public Node(int x)
        {
            data = x;
            parent = null;
            left = null;
            right = null;
        }
        public Node Right 
        {
            get { return right; }
            set { right = value; }
        }
        public Node Left
        {
            get { return left; }
            set { left = value; }
        }
        public Node Parent
        {
            get { return parent; }
            set { parent = value; }
        }
        public int  Data
        {
            get { return data; }
            set { data = value; }
        }

    }
}
