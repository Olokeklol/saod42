﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod42
{
    class AVLTree
    {
        public AVLNode root;
        public int Height(AVLNode N)
        {
            return N == null ? 0 : N.height;
        }
        public AVLNode leftRotate(AVLNode x)
        {
            AVLNode right = x.right;
            AVLNode rightleft = right.left;
            right.left = x;
            x.right = rightleft;
            if (x == root) root = right;
            x.height = Math.Max(Height(x.left), Height(x.right)) + 1;
            right.height = Math.Max(Height(right.left), Height(right.right)) + 1;
            return right;
        }
        public int getBalance(AVLNode q)
        {
            return q == null ? 0 : Height(q.left) - Height(q.right);
        }

        public AVLNode insert(AVLNode q, int x)
        {
            if (q == null) return (new AVLNode(x));
            if (x < q.data)
                q.left = insert(q.left, x);
            else
                q.right = insert(q.right, x);

            q.height = 1 + Math.Max(Height(q.left), Height(q.right));
            int b = getBalance(q);

            if (b < -1 && q.right.data < x)
                leftRotate(q);

            return q;
        }


        public List<List<int>> get_lists()
        {
            var lst = new List<List<int>>();
            lst.Add(new List<int>());
            print_tree(root, 0, lst);
            return lst;
        }
        public void print_tree(AVLNode n, int step, List<List<int>> lst)
        {
            if (n.right != null)
            {
                print_tree(n.right, step + 1, lst);
            }
            for (int i = 0; i < step; i++)
                lst.Last().Add(-1);
            lst.Last().Add(n.data);
            lst.Add(new List<int>());
            if (n.left != null)
            {
                print_tree(n.left, step + 1, lst);
            }
        }
        public List<string> get_tree_list()
        {
            List<string> rez = new List<string>();
            var lst = get_lists();
            int max = lst.Max(obj => obj.Count);
            for (int i = 0; i < lst.Count; i++)
            {
                var t = Enumerable.Repeat(-1, max - lst[i].Count).ToList();
                lst[i].AddRange(t);
            }
            var temp = new List<List<int>>();
            for (int i = 0; i < max; i++)
            {
                temp.Add(Enumerable.Repeat(-1, lst.Count).ToList());
            }
            for (int count = 0; count < lst.Count; count++)
            {
                for (int count2 = 0; count2 < max; count2++)
                {
                    temp[count2][lst.Count - count - 1] = lst[count][count2];
                }
            }
            lst = temp;
            int maxValueLength = lst.Max(row => row.Max()).ToString().Length;
            for (int i = 0; i < lst.Count; i++)
            {
                var tt = new System.Text.StringBuilder();
                if (lst[i].Max() < 0)
                    continue;
                for (int j = 0; j < temp[i].Count; j++)
                {
                    if (lst[i][j] == -1)
                    {
                        tt.Append(string.Concat(Enumerable.Repeat(' ', maxValueLength)).ToCharArray());
                    }
                    else
                    {
                        tt.Append(lst[i][j].ToString());
                        tt.Append(string.Concat(Enumerable.Repeat(' ', maxValueLength - lst[i][j].ToString().Length)).ToCharArray());
                    }
                }
                rez.Add(tt.ToString());
            }
            return rez;
        }


    }
}
