﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod42
{
    class AVLNode
    {
        public AVLNode left;
        public AVLNode right;
        public int data;
        public int height;

        public AVLNode()
        {
            left = right = null;
        }
        public AVLNode(int x)
        {
            left = right = null;
            data = x;
            height = 1;
        }
    }
}
