﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace saod42
{
    class RBTree
    {
        private Node root;

        public void rotateleft(ref Node rt,Node q)
        {
            Node q_r = q.Right;
            q.Right = q.Right.Left;
            if(q.Right != null)
            {
                q.Right.Parent = q;
            }
            q_r.Parent = q.Parent;
            if (q.Parent==null)
            {
                rt = q_r;
            }
            else if (q.Parent.Left==q)
            {
                q.Parent.Left = q_r;
            }
            else
            {
                q.Parent.Right = q_r;
            }
            q_r.Left = q;
            q.Parent = q_r;

        }

        public Node Root
        {
            get { return root; }
            set { root = value; }
        }
        public RBTree()
        {
            root = null;
        }
        public void insert (int x)
        {
            Node q = new Node(x);
            //q.color = Mark.Red;
            root = insert(root, q);
        }
        private Node insert(Node rt,Node q)
        {
            if (rt == null)
            {
                return q;
            }
            if (q.Data<rt.Data)
            {
                rt.Left = insert(rt.Left, q);
                rt.Left.Parent = rt;
            }
            else if (q.Data > rt.Data)
            {
                rt.Right = insert(rt.Right, q);
                rt.Right.Parent = rt;
            }
            return rt;
        }

        public List<List<Tuple<int, Mark>>> get_lists()
        {
            var lst = new List<List<Tuple<int, Mark>>>();
            lst.Add(new List<Tuple<int, Mark>>());
            print_tree(root, 0, lst);
            return lst;
        }
        public void print_tree(Node n, int step, List<List<Tuple<int, Mark>>> lst)
        {
            if (n.Right != null)
            {
                print_tree(n.Right, step + 1, lst);
            }
            for (int i = 0; i < step; i++)
                lst.Last().Add(new Tuple<int, Mark>(-1, Mark.Black));
            lst.Last().Add(new Tuple<int, Mark>(n.Data, n.color));
            lst.Add(new List<Tuple<int, Mark>>());
            if (n.Left != null)
            {
                print_tree(n.Left, step + 1, lst);
            }
        }
        public List<List<Tuple<string, Mark>>> get_tree_list()
        {
            var rez = new List<List<Tuple<string, Mark>>>();
            var lst = get_lists();
            int max = lst.Max(obj => obj.Count);
            for (int i = 0; i < lst.Count; i++)
            {
                var t = Enumerable.Repeat(-1, max - lst[i].Count).ToList();
                var tt = new List<Tuple<int, Mark>>();

                foreach(var j in t)
                {
                    tt.Add(new Tuple<int, Mark>(j, Mark.Black));
                }

                lst[i].AddRange(tt);
            }
            var temp = new List<List<Tuple<int, Mark>>>();
            for (int i = 0; i < max; i++)
            {
                var t = Enumerable.Repeat(-1, lst.Count).ToList();

                var tt = new List<Tuple<int, Mark>>();

                foreach (var j in t)
                {
                    tt.Add(new Tuple<int, Mark>(j, Mark.Black));
                }

                temp.Add(tt);
            }
            for (int count = 0; count < lst.Count; count++)
            {
                for (int count2 = 0; count2 < max; count2++)
                {
                    temp[count2][lst.Count - count - 1] = lst[count][count2];
                }
            }
            lst = temp;
            int maxValueLength = 2;//lst.Max(row => row.Max()).ToString().Length;
            for (int i = 0; i < lst.Count; i++)
            {
                var tt = new  List<Tuple<string, Mark>>();

                int max2 = -1;
                
                for(int j = 0; j != lst[i].Count; j++)
                {
                    if (max2 < lst[i][j].Item1)
                        max2 = lst[i][j].Item1;
                }
                if (max2 < 0)
                    continue;
                for (int j = 0; j < temp[i].Count; j++)
                {
                    if (lst[i][j].Item1 == -1)
                    {
                        tt.Add(new Tuple<string, Mark>(string.Concat(Enumerable.Repeat(' ', maxValueLength)), Mark.Black));
                        //tt.Append(string.Concat(Enumerable.Repeat(' ', maxValueLength)).ToCharArray());
                    }
                    else
                    {
                        tt.Add(new Tuple<string, Mark>(lst[i][j].Item1.ToString(), lst[i][j].Item2));
                        tt.Add(new Tuple<string, Mark>(string.Concat(Enumerable.Repeat(' ', maxValueLength - lst[i][j].Item1.ToString().Length)), Mark.Black));


                        //tt.Append(lst[i][j].ToString());
                        //tt.Append(string.Concat(Enumerable.Repeat(' ', maxValueLength - lst[i][j].ToString().Length)).ToCharArray());
                    }
                }
                rez.Add(tt);
            }
            return rez;
        }
    }
}
