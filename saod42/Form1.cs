﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace saod42
{
    public partial class Form1 : Form
    {
        private AVLTree tree;
        public Form1()
        {
            InitializeComponent();
            tree = new AVLTree();
            tree.root = new AVLNode(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
                        
                tree.insert(tree.root, Convert.ToInt32(textBox2.Text));

            //tree.leftRotate(tree.root.right);


            var rez = tree.get_tree_list();
            richTextBox1.Multiline = true;
            richTextBox1.Clear();
            richTextBox1.Lines = rez.ToArray();
        }
        

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
